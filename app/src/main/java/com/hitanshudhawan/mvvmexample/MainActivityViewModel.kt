package com.hitanshudhawan.mvvmexample

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.hitanshudhawan.mvvmexample.network.ApiClient
import com.hitanshudhawan.mvvmexample.network.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivityViewModel : ViewModel() {

    private val _repositories = MutableLiveData<List<Repository>>()
    val repositories: LiveData<List<Repository>>
        get() = _repositories

    private val _isFetchingRepositories = MutableLiveData<Boolean>()
    val isFetchingRepositories: LiveData<Boolean>
        get() = _isFetchingRepositories

    fun getUserRepositories() {
        _repositories.value = emptyList()
        _isFetchingRepositories.value = true

        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        apiService.getUserRepositories("hitanshu-dhawan").enqueue(object : Callback<List<Repository>> {

            override fun onResponse(call: Call<List<Repository>>, response: Response<List<Repository>>) {
                if (response.isSuccessful) {
                    _repositories.value = response.body()
                    _isFetchingRepositories.value = false
                }
            }

            override fun onFailure(call: Call<List<Repository>>, t: Throwable) {
                //
            }
        })
    }

}