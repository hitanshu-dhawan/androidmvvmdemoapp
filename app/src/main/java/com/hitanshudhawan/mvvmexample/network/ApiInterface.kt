package com.hitanshudhawan.mvvmexample.network

import com.hitanshudhawan.mvvmexample.Repository
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {

    @GET("users/{username}/repos")
    fun getUserRepositories(@Path("username") username: String): Call<List<Repository>>

}