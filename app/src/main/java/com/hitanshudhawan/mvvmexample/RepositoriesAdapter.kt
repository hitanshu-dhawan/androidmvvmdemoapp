package com.hitanshudhawan.mvvmexample

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class RepositoriesAdapter(private val context: Context, private val repositories: List<Repository>) :
    RecyclerView.Adapter<RepositoriesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_repository, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.repositoryTextView.text = repositories[position].fullName
        holder.repositoryLanguageTextView.text = repositories[position].language
    }

    override fun getItemCount() = repositories.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val repositoryTextView = itemView.findViewById<TextView>(R.id.repository_text_view)!!
        val repositoryLanguageTextView = itemView.findViewById<TextView>(R.id.repository_language_text_view)!!
    }
}