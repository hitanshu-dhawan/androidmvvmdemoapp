package com.hitanshudhawan.mvvmexample

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

// https://youtu.be/ijXjCtCXcN4
// https://resocoder.com/2018/08/31/introduction-to-mvvm-on-android/
class MainActivity : AppCompatActivity() {

    private val mainActivityViewModel by lazy {
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        initViews()
    }

    private fun initViews() {

        recycler_view.apply {
            adapter = RepositoriesAdapter(this@MainActivity, emptyList())
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        fab.setOnClickListener {
            mainActivityViewModel.getUserRepositories()
        }

        mainActivityViewModel.repositories.observe(this, Observer {
            if (it != null)
                recycler_view.adapter = RepositoriesAdapter(this@MainActivity, it)
        })

        mainActivityViewModel.isFetchingRepositories.observe(this, Observer {
            if (it != null)
                progress_bar.visibility = if (it) View.VISIBLE else View.GONE
        })
    }
}
